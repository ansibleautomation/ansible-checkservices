## Service Report

Simple Ansible Playbook to collect list of running services on the host and kill them.

Service list can be customized in the file group_vars/all


> Usage: 

`ansible-playbook -i hosts run.yml`